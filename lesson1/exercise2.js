const answer1 = +prompt('Введите первое число', 0);
let answer2;

if (isNaN(answer1)) {
  console.log('Некорректный ввод!');
} else {
  answer2 = +prompt('Введите второе число', 0);

  if (isNaN(answer2)) {
    console.log('Некорректный ввод!');
  } else {
    console.log(`Ответ: ${answer1 + answer2}, ${answer1 / answer2}`);
  }
}

