function createDebounceFunction(callbackFn, delay) {
  let timer;
  return function (...args) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      callbackFn.apply(this, args);
    }, delay);
  };
}

const log100 = () => console.log(100);
const debounceLog100 = createDebounceFunction(log100, 3000);
debounceLog100();
setTimeout(debounceLog100, 1000);
setTimeout(debounceLog100, 500); 
