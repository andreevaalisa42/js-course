Array.prototype.myFilter = function (callback, contextObj) { // По умолчанию - контекст = undefined
  const filtered = [];

  if (this == null) {
    throw new Error("Can't iterate over null");
  }

  for (let i = 0; i < this.length; i++) {
    if (callback.call(contextObj, this[i], i, this)) {
      filtered.push(this[i]);
    }
  }

  return filtered;

};

// Объект для примера
const object = {
  name: 'Alex',
  age: 22,
  occupation: 'Job'
};

// callback для примера
const fnCallback = function(x) {
  return x % 2 === 0;
};

// Пример с прривязкой контекста
const arr = [1, 2, 3, 5, 6, 99, 4, 98];
console.log(arr.myFilter(fnCallback, object));
