'use strict';

function concatStrings(string, separator) {
  const validString = checkString(string) ? string : '';
  const validSeparator = checkString(separator) ? separator : '';
  const result = [validString];

  function fn(string) {
    if (!checkString(string)) {
      return result.join(validSeparator);
    }
    result.push(string);
    return fn;
  }
  return fn;
}

function checkString(string) {
  return typeof string === 'string' ? true : false;
}

// Примеры
console.log(concatStrings('first')('second')('third')());
console.log(concatStrings('first', null)('second')());
console.log(concatStrings('first', '123')('second')('third')());
console.log(concatStrings('some-value')('')('')(null));
console.log(concatStrings('some-value')(2));
console.log(concatStrings('some-value')('333')(123n));
