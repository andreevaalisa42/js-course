class Calculator {
  constructor(...args) {
    if (args.length === 2) {
      this.setX(args[0]);
      this.setY(args[1]);
    } else {
      throw new Error('Конструктор класса Calculator принимает 2 параметра');
    }
    this.logSum = this.logSum.bind(this);
    this.logMul = this.logMul.bind(this);
    this.logSub = this.logSub.bind(this);
    this.logDiv = this.logDiv.bind(this);
  }

  checkValue(value) {
    if (value === null || isNaN(value) || !isFinite(value) || typeof value === 'bigint') {
      throw new Error('Значение должно быть валидным числом.');
    } else {
      return true;
    }
  }

  setX(value) {
    if (typeof value === 'number' && this.checkValue(value)) {
      this.x = value;
    } else {
      throw new Error('Значение не является валидным числом');
    }
  }

  setY(value) {
    if (typeof value === 'number' && this.checkValue(value)) {
      this.y = value;
    } else {
      throw new Error('Значение не является валидным числом');
    }
  }

  logSum() {
    console.log(this.x + this.y);
  }

  logMul() {
    console.log(this.x * this.y);
  }

  logSub() {
    console.log(this.x - this.y);
  }

  logDiv() {
    if (this.y === 0) {
      throw new Error('На ноль делить нельзя');
    } else {
      console.log(this.x / this.y);
    }
  }
}

// Примеры
const calculator = new Calculator(12, 3);
calculator.logSum(); // 15
calculator.logDiv(); // 4
calculator.setX(15);
calculator.logDiv(); // 5
const logCalculatorDiv = calculator.logDiv;
logCalculatorDiv(); // 5
calculator.setY(444n); // Ошибка!
