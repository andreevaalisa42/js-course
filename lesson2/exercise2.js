'use strict';

const selectFromInterval = (arrOfNumbers, firstNum, secondNum) => {
  let intervalStart, intervalEnd;
  let outputArray = [];

  if (!onlyNumbersCheck(arrOfNumbers)) {
    throw new Error('В качестве первого параметра передан не массив или массив, содержащий не только числа');
  } else if (typeof firstNum !== 'number' || typeof secondNum !== 'number') {
    throw new Error('В качестве значений интервала были переданы не числа');
  } // проводим проверки на валидность входящих данных

  if (firstNum <= secondNum) { // определяем начало и конец интервала
    intervalStart = firstNum;
    intervalEnd = secondNum;
  } else if (firstNum > secondNum) {
    intervalStart = secondNum;
    intervalEnd = firstNum; 
  }
  
  arrOfNumbers.forEach(number => {
    if (number >= intervalStart && number <= intervalEnd) {
      outputArray.push(number); 
    }
  });

  return outputArray;
};

// функция, проверяющая, что все элементы массива - числа
const onlyNumbersCheck = (array) => {
  if (Array.isArray(array)) {
    return array.every(el => {
      return typeof el === 'number';
    });
  } else {
    return false;
  }
};

// проверки
console.log(selectFromInterval([1,2,3,4,5,6], 4, 4)); 
console.log(selectFromInterval([1,3,5], 5, 2));
console.log(selectFromInterval([-2, -15, 0, 4], -13, -5));
console.log(selectFromInterval(['aaa'], 2, 3));
