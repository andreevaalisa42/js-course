'use strict';

const makeObjectDeepCopy = (inputObject) => {
  if (typeof inputObject !== 'object' || inputObject === null) {
    return inputObject; // базовый случай
  }

  const outputObject = Array.isArray(inputObject) ? [] : {};

  for (const key in inputObject) {
    const value = inputObject[key];
    outputObject[key] = makeObjectDeepCopy(value);
  }

  return outputObject;
};
