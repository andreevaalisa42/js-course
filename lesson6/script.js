const textIcons = document.querySelectorAll('.textIcon');

textIcons.forEach(icon => {
    const textEl = icon.previousElementSibling;
    const parentEl = icon.parentElement;
    
    handleResize(icon, textEl);
    const observer = new ResizeObserver((entries) => {
        handleResize(icon, textEl);
    });
    observer.observe(parentEl);
});

function handleResize(icon, text) {
    icon.style.left = text.offsetWidth + 16 + 'px';
}
